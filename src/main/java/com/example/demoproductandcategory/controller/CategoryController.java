package com.example.demoproductandcategory.controller;

import com.example.demoproductandcategory.constan.Constants;
import com.example.demoproductandcategory.models.Category;
import com.example.demoproductandcategory.repository.CategoryRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class CategoryController {
    private final CategoryRepository categoryRepository;

    public CategoryController(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }
    @GetMapping("/categories")
    public String index(Model model){
        List<Category> list = categoryRepository.findAllByStatusInOrderByIdDesc(Constants.getAllStatusString());
        model.addAttribute("categories", list);
        System.out.println(list);
        return "category/index";
    }

    @GetMapping("/categories/create")
    public String create(Model model){
        Category category  =  new Category();
        category.setStatuslist(Constants.getAllStatus());
        model.addAttribute("category",category);
        return "category/create";
    }

    @PostMapping("/categories/create")
    public String create(Model model, @ModelAttribute("category")Category category){
        if(category.getName() == ""){
            return "category/create";
        }
        categoryRepository.save(category);
        return "redirect:/categories";
    }

    @GetMapping("/categories/edit/{id}")
    public String edit(@PathVariable("id") Integer id, Model model){
        Category category = categoryRepository.findById(id).orElse(null);
        if(category != null) {
            category.setStatuslist(Constants.getAllStatus());
            model.addAttribute("category", category);
            return "category/create";
        }
        return "redirect:/categories";
    }

    @GetMapping("/categories/delete/{id}")
    public String delete(@PathVariable("id") Integer id){
        Category find = categoryRepository.findById(id).orElse(null);
        if(find != null) {
            find.setStatus("DEL");
            categoryRepository.save(find);
        }
        return "redirect:/categories";
    }
    @GetMapping("/categories/disable/{id}")
    public String disable(@PathVariable("id") Integer id){
        Category find = categoryRepository.findById(id).orElse(null);
        if(find != null) {
            find.setStatus("DSL");
            categoryRepository.save(find);
        }
        return "redirect:/categories";
    }
    @GetMapping("/categories/active/{id}")
    public String active(@PathVariable("id") Integer id){
        Category find = categoryRepository.findById(id).orElse(null);
        if(find != null) {
            find.setStatus("ACT");
            categoryRepository.save(find);
        }
        return "redirect:/categories";
    }

}
