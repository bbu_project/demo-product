package com.example.demoproductandcategory.models;

import com.example.demoproductandcategory.models.request.ItemKeyValue;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
@Entity
@Table(name = "categories")
@ToString
@Getter
@Setter
public class Category {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private int id;
        private String name;
        private String status;
        @Transient
        private List<ItemKeyValue> statuslist;

}
